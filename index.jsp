<%-- 
    Document   : index
    Created on : 14.12.2021, 18:32:04
    Author     : Tatiana
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

        <div id="main">
            <aside class="leftAside">
                <h2>Товары</h2>
                <ul>
                    <li><a href="#">Моющие средства</a></li>
                    <li><a href="#">Средства для чистки</a></li>
                    <li><a href="#">Дезинфекционные средства</a></li>
                    <li><a href="#">Репелленты</a></li>
                    
                </ul>
            </aside>
            <section>
                <article>
                    <h1>Хиты продаж</h1>
                    <div class="text-article">
                        Чистящий порошок Комет Лимон в банке 475г
                    </div>
                    <div class="fotter-article">
                        <span class="autor">Поставщик: <a href="#">Волкова</a></span>
                        <span class="read"><a href="javascript:void(0);">Заказать...</a></span>
                        <span class="date-article">Цена: 38 р</span>
                    </div>
                </article>
                <article>
                    <h1>Хиты продаж</h1>
                    <div class="text-article">
                        Стиральный порошок Ушастый Нянь 4,5 кг
                    </div>
                    <div class="fotter-article">
                        <span class="autor">Поставщик: <a href="#">Волкова</a></span>
                        <span class="read"><a href="javascript:void(0);">Заказать...</a></span>
                        <span class="date-article">Цена: 448 р</span>
                        
                    </div>
                </article>
            </section>
        </div>
